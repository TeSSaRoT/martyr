package com.example.itrysohardgotsofar

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.itrysohardgotsofar.databinding.RecViewFragmentBinding

class recViewFragment : Fragment() {

    companion object {
        fun newInstance() = recViewFragment()
    }
    lateinit var binding: RecViewFragmentBinding
    lateinit var adapter: PersonListElemAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = RecViewFragmentBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        adapter = PersonListElemAdapter()
        adapter.setOnPersonActionListener(object : PersonListElemAdapter.onPersonActionListener{
            override fun onPersonClick(title: String, description: String) {
                val bundle = Bundle()
                bundle.putString("twTitle",title)
                bundle.putString("twDescription",description)
                if(requireActivity() is MainActivity){
                    findNavController().navigate(R.id.action_recViewFragment_to_fragmentTwo,bundle)
                }
            }

        })
        init()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    private fun init(){
        binding.rcView.layoutManager = LinearLayoutManager(context)
        binding.rcView.adapter = adapter
        for(i in 1..1000){
            val person = PersonListElem("Title $i","Description $i")
            adapter.addPersons(person)
        }
    }
}