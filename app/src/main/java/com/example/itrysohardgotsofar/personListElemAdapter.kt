package com.example.itrysohardgotsofar

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.itrysohardgotsofar.databinding.PersonElementBinding


class PersonListElemAdapter:RecyclerView.Adapter<PersonListElemAdapter.PersonHolder>(){

    val personList = ArrayList<PersonListElem>()

    class PersonHolder(item: View,listener: onPersonActionListener):RecyclerView.ViewHolder(item){
        val binding = PersonElementBinding.bind(item)

        fun bind (person:PersonListElem) = with(binding){
            textView.text = person.title
            textView2.text = person.description
        }

        init {
            itemView.setOnClickListener {
                val personTitle = binding.textView.text.toString()
                val personDescription = binding.textView2.text.toString()
                listener.onPersonClick(personTitle,personDescription)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PersonHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.person_element,parent,false)
        return PersonHolder(view,mListener)
    }

    override fun onBindViewHolder(holder: PersonHolder, position: Int) {
        val person = personList[position]
        holder.binding.textView.tag = person
        holder.bind(personList[position])
    }

    override fun getItemCount(): Int {
        return personList.size
    }

    fun addPersons(person: PersonListElem){
        personList.add(person)
        notifyDataSetChanged()
    }




    private lateinit var mListener: onPersonActionListener
    interface onPersonActionListener{
        fun onPersonClick(title: String,description: String)
    }

    fun setOnPersonActionListener(listener: onPersonActionListener){
        mListener = listener
    }
}