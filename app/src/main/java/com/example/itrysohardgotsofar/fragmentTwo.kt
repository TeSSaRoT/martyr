package com.example.itrysohardgotsofar

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.itrysohardgotsofar.databinding.FragmentTwoBinding
import android.widget.ImageButton

class FragmentTwo:Fragment() {

    private lateinit var binding:FragmentTwoBinding

    override fun onCreateView(inflater: LayoutInflater, container:ViewGroup?, savedInstantState: Bundle?):View {
        binding = FragmentTwoBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val title : TextView = view.findViewById(R.id.textView4)
        val description : TextView = view.findViewById(R.id.textView5)
        val titleText = arguments?.getString("twTitle")
        val descriptionText = arguments?.getString("twDescription")
        title.text = titleText
        description.text = descriptionText
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val backButton: ImageButton = activity?.findViewById(R.id.imageButton) as ImageButton
        backButton.setOnClickListener {
            activity?.onBackPressed()
        }
        val closeButton: ImageButton = activity?.findViewById(R.id.imageButton2) as ImageButton
        closeButton.setOnClickListener {
            activity?.finish()
        }
    }
}